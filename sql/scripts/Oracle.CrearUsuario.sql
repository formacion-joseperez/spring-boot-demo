﻿--
-- Este Script crea en LOCAL el usuario
--
-- Despues de cambiar usuario y contraseña, ejecutarlo con:
--
-- sqlplus -L "/ as sysdba" @ Oracle.CrearUsuario.sql
--

CREATE USER tiendadb IDENTIFIED BY tiendadb_PASS1234;
GRANT CREATE SESSION, ALTER SESSION, ALTER USER TO tiendadb;
GRANT CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO tiendadb;
GRANT "RESOURCE" TO "tiendadb";
GRANT "CONNECT" TO "tiendadb";
GRANT CREATE TYPE TO tiendadb;

--GRANTS NECESARIOS PARA QUE FUNCIONES LOS TRIGGERS DE LAS TABLAS CTRL
grant select on sys.v_$mystat  to tiendadb;
grant select on v_$session to tiendadb;

exit;