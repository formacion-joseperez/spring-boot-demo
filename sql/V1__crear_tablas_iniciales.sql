
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET default_storage_engine=InnoDB;


CREATE TABLE IF NOT EXISTS carritos (
  id_usuario INT(11) NOT NULL,
  carrito    TEXT,
  PRIMARY KEY (id_usuario)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

  
CREATE TABLE IF NOT EXISTS categorias (
  id_categoria   INT(11) NOT NULL AUTO_INCREMENT,
  padre          INT(11)          DEFAULT NULL,
  nombre         VARCHAR(45)      DEFAULT NULL,
  activo         TINYINT(1)       DEFAULT NULL,
  fecha_creacion DATETIME         DEFAULT NULL,
  PRIMARY KEY (id_categoria),
  KEY fk_categorias_categorias1_idx (padre)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 14;


CREATE TABLE IF NOT EXISTS colores (
  id_color INT(11) NOT NULL AUTO_INCREMENT,
  nombre   VARCHAR(45)      DEFAULT NULL,
  rgb      VARCHAR(7)       DEFAULT NULL,
  PRIMARY KEY (id_color)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 7;


CREATE TABLE IF NOT EXISTS estadospedido (
  id_estado   INT(11) NOT NULL AUTO_INCREMENT,
  nombre      VARCHAR(45)      DEFAULT NULL,
  descripcion VARCHAR(200)     DEFAULT NULL,
  PRIMARY KEY (id_estado)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;


CREATE TABLE IF NOT EXISTS formasdepago (
  id_formadepago INT(11) NOT NULL AUTO_INCREMENT,
  nombre         VARCHAR(45)      DEFAULT NULL,
  activo         TINYINT(1)       DEFAULT NULL,
  fecha_creacion DATETIME         DEFAULT NULL,
  PRIMARY KEY (id_formadepago)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 5;


CREATE TABLE IF NOT EXISTS fotosproductos (
  id_foto     INT(11) NOT NULL AUTO_INCREMENT,
  producto_id INT(11) NOT NULL,
  PRIMARY KEY (id_foto, producto_id),
  KEY fk_fotosproductos_productos1_idx (producto_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

  
CREATE TABLE IF NOT EXISTS grupos (
  id_grupo       INT(11) NOT NULL AUTO_INCREMENT,
  nombre         VARCHAR(45)      DEFAULT NULL,
  descripcion    VARCHAR(200)     DEFAULT NULL,
  activo         TINYINT(1)       DEFAULT NULL,
  fecha_creacion DATETIME         DEFAULT NULL,
  PRIMARY KEY (id_grupo)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 4;


CREATE TABLE IF NOT EXISTS lineasdepedido (
  pedidos_pedido_id     INT(11) NOT NULL,
  productos_id_producto INT(11) NOT NULL,
  cantidad              INT(11)        DEFAULT NULL,
  precio                DECIMAL(10, 2) DEFAULT NULL,
  PRIMARY KEY (pedidos_pedido_id, productos_id_producto),
  KEY fk_lineasdepedido_productos1_idx (productos_id_producto)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE IF NOT EXISTS listasdeseos (
  id_usuario     INT(11) NOT NULL,
  id_producto    INT(11) NOT NULL,
  activo         TINYINT(1) DEFAULT NULL,
  fecha_creacion DATETIME   DEFAULT NULL,
  PRIMARY KEY (id_usuario, id_producto),
  KEY fk_listasdeseos_productos1_idx (id_producto)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

 
CREATE TABLE IF NOT EXISTS marcas (
  id_marca INT(11) NOT NULL AUTO_INCREMENT,
  nombre   VARCHAR(45)      DEFAULT NULL,
  url      VARCHAR(60)      DEFAULT NULL,
  avatar   TINYINT(1)       DEFAULT NULL,
  PRIMARY KEY (id_marca)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 4;


CREATE TABLE IF NOT EXISTS pedidos (
  pedido_id         INT(11) NOT NULL AUTO_INCREMENT,
  id_usuario        INT(11) NOT NULL,
  formadepago_id    INT(11) NOT NULL,
  estado_id         INT(11) NOT NULL,
  referencia        VARCHAR(45)      DEFAULT NULL,
  fecha_creacion    DATETIME         DEFAULT NULL,
  fecha_pago        DATETIME         DEFAULT NULL,
  fecha_cancelacion DATETIME         DEFAULT NULL,
  PRIMARY KEY (pedido_id),
  KEY fk_pedidos_formasdepago1_idx (formadepago_id),
  KEY fk_pedidos_estadospedido1_idx (estado_id),
  KEY fk_pedidos_usuarios1_idx (id_usuario)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

  
CREATE TABLE IF NOT EXISTS productos (
  id_producto    INT(11) NOT NULL AUTO_INCREMENT,
  categoria_id   INT(11) NOT NULL,
  marca_id       INT(11) NOT NULL,
  color_id       INT(11) NOT NULL,
  nombre         VARCHAR(45)      DEFAULT NULL,
  descripcion    TEXT,
  talla          INT(11)          DEFAULT NULL,
  precio         DECIMAL(10, 2)   DEFAULT NULL,
  stock          INT(11)          DEFAULT NULL,
  es_outlet      TINYINT(1)       DEFAULT NULL,
  es_oferta      TINYINT(1)       DEFAULT NULL,
  activo         TINYINT(1)       DEFAULT NULL,
  fecha_creacion DATETIME         DEFAULT NULL,
  PRIMARY KEY (id_producto),
  KEY fk_productos_marcas1_idx (marca_id),
  KEY fk_productos_colores1_idx (color_id),
  KEY fk_productos_categorias1_idx (categoria_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

  
CREATE TABLE IF NOT EXISTS usuarios (
  id_usuario       INT(11) NOT NULL AUTO_INCREMENT,
  login            VARCHAR(45)      DEFAULT NULL,
  `password`       VARCHAR(255)     DEFAULT NULL,
  avatar           TINYINT(1)       DEFAULT '0',
  nombre           VARCHAR(60)      DEFAULT NULL,
  email            VARCHAR(60)      DEFAULT NULL,
  direccion        TEXT,
  nif              VARCHAR(12)      DEFAULT NULL,
  activo           TINYINT(1)       DEFAULT NULL,
  fecha_nacimiento DATE             DEFAULT NULL,
  fecha_creacion   DATETIME         DEFAULT NULL,
  PRIMARY KEY (id_usuario),
  UNIQUE KEY login_UNIQUE (login)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 4;


CREATE TABLE IF NOT EXISTS usuariosgrupos (
  id_usuario INT(11) NOT NULL,
  id_grupo   INT(11) NOT NULL,
  PRIMARY KEY (id_usuario, id_grupo),
  KEY fk_usuarios_has_grupos_grupos1_idx (id_grupo),
  KEY fk_usuarios_has_grupos_usuarios_idx (id_usuario)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

  
--
-- Creo claves ajenas
--  


ALTER TABLE carritos
  ADD CONSTRAINT fk_carritos_usuarios1 FOREIGN KEY (id_usuario) REFERENCES usuarios (id_usuario)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
ALTER TABLE categorias
  ADD CONSTRAINT fk_categorias_categorias1 FOREIGN KEY (padre) REFERENCES categorias (id_categoria)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
ALTER TABLE fotosproductos
  ADD CONSTRAINT fk_fotosproductos_productos1 FOREIGN KEY (producto_id) REFERENCES productos (id_producto)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
ALTER TABLE lineasdepedido
  ADD CONSTRAINT fk_lineasdepedido_pedidos1 FOREIGN KEY (pedidos_pedido_id) REFERENCES pedidos (pedido_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_lineasdepedido_productos1 FOREIGN KEY (productos_id_producto) REFERENCES productos (id_producto)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
ALTER TABLE listasdeseos
  ADD CONSTRAINT fk_listasdeseos_productos1 FOREIGN KEY (id_producto) REFERENCES productos (id_producto)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_listasdeseos_usuarios1 FOREIGN KEY (id_usuario) REFERENCES usuarios (id_usuario)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
ALTER TABLE pedidos
  ADD CONSTRAINT fk_pedidos_estadospedido1 FOREIGN KEY (estado_id) REFERENCES estadospedido (id_estado)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_pedidos_formasdepago1 FOREIGN KEY (formadepago_id) REFERENCES formasdepago (id_formadepago)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_pedidos_usuarios1 FOREIGN KEY (id_usuario) REFERENCES usuarios (id_usuario)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
ALTER TABLE productos
  ADD CONSTRAINT fk_productos_categorias1 FOREIGN KEY (categoria_id) REFERENCES categorias (id_categoria)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_productos_colores1 FOREIGN KEY (color_id) REFERENCES colores (id_color)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_productos_marcas1 FOREIGN KEY (marca_id) REFERENCES marcas (id_marca)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
ALTER TABLE usuariosgrupos
  ADD CONSTRAINT fk_usuarios_has_grupos_grupos1 FOREIGN KEY (id_grupo) REFERENCES grupos (id_grupo)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_usuarios_has_grupos_usuarios FOREIGN KEY (id_usuario) REFERENCES usuarios (id_usuario)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


