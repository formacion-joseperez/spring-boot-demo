
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


INSERT INTO usuarios (id_usuario, login, password, avatar, nombre, email, direccion, nif, activo, fecha_nacimiento, fecha_creacion)
VALUES
  (1, 'admin', 'admin', 0, 'admin', NULL, NULL, NULL, 1, NULL, '2018-03-07 15:39:08'),
  (2, 'roberto', '123456', 0, 'roberto', NULL, NULL, NULL, 1, NULL, '2018-03-07 15:39:08'),
  (3, 'joana', '123456', 0, 'joana', NULL, NULL, NULL, 1, NULL, '2018-03-07 15:39:08');

  
INSERT INTO grupos (id_grupo, nombre, descripcion, activo, fecha_creacion) VALUES
  (1, 'admin', NULL, 1, '2018-03-07 15:36:06'),
  (2, 'empleados', NULL, 1, '2018-03-07 15:36:06'),
  (3, 'clientes', NULL, 1, '2018-03-07 15:36:06');
  

INSERT INTO usuariosgrupos (id_usuario, id_grupo) VALUES
  (1, 1),
  (3, 1),
  (2, 2),
  (3, 3);
  

INSERT INTO colores (id_color, nombre, rgb) VALUES
  (1, 'rojo', '#FF0000'),
  (2, 'azul', '#0000FF'),
  (3, 'verde', '#00FF00'),
  (4, 'amarillo', '#FFFF00'),
  (5, 'verde', '#008000'),
  (6, 'negro', '#000000');

  
INSERT INTO estadospedido (id_estado, nombre, descripcion) VALUES
  (1, 'creado', NULL),
  (2, 'en curso', NULL),
  (3, 'entregado', NULL),
  (4, 'cancelado', NULL);


INSERT INTO formasdepago (id_formadepago, nombre, activo, fecha_creacion) VALUES
  (1, 'tarjeta de credito', 1, '2018-03-07 15:18:09'),
  (2, 'cheque bancario', 0, '2018-03-07 15:18:09'),
  (3, 'transferencia bancaria', 1, '2018-03-07 15:18:09'),
  (4, 'contrareembolso', 1, '2018-03-07 15:18:09');


INSERT INTO marcas (id_marca, nombre, url, avatar) VALUES
  (1, 'Zara', NULL, NULL),
  (2, 'Woman Secret', NULL, NULL),
  (3, 'New Look', NULL, NULL);  
   
  
INSERT INTO categorias (id_categoria, padre, nombre, activo, fecha_creacion) VALUES
  (1, NULL, 'hombre', 1, NOW()),
  (2, 1, 'camisas', 1, NOW()),
  (3, 1, 'pantalones', 1, NOW()),
  (4, 1, 'zapatos', 1, NOW()),
  (5, 1, 'complementos', 1, NOW()),
  (6, NULL, 'mujer', 1, NOW()),
  (7, 6, 'camisas', 1, NOW()),
  (8, 6, 'pantalones', 1, NOW()),
  (9, 6, 'zapatos', 1, NOW()),
  (10, 9, 'tacon', 1, NOW()),
  (11, 9, 'botas', 1, NOW()),
  (12, 9, 'bailarinas', 1, NOW()),
  (13, 6, 'complementos', 1, NOW());

