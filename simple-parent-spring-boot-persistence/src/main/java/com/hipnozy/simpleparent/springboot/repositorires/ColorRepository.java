package com.hipnozy.simpleparent.springboot.repositorires;

import com.hipnozy.simpleparent.springboot.entities.Color;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ColorRepository extends JpaRepository<Color, Long> {
}
