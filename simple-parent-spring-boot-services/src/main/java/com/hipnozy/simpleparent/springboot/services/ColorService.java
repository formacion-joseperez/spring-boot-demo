package com.hipnozy.simpleparent.springboot.services;

import com.hipnozy.simpleparent.springboot.entities.Color;

import java.util.List;

public interface ColorService {
    List<Color> getAllColors();
}
