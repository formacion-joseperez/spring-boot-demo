package com.hipnozy.simpleparent.springboot.services.impl;

import com.hipnozy.simpleparent.springboot.entities.Color;
import com.hipnozy.simpleparent.springboot.repositorires.ColorRepository;
import com.hipnozy.simpleparent.springboot.services.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColorServiceImpl implements ColorService {

    @Autowired
    private ColorRepository colorRepository;

    private ColorServiceImpl() {
    }

    @Override
    public List<Color> getAllColors() {
        return colorRepository.findAll();
    }
}
