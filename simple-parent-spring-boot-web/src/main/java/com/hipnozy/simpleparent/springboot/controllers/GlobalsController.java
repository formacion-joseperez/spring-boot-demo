package com.hipnozy.simpleparent.springboot.controllers;

import com.hipnozy.simpleparent.springboot.enums.StatesEnum;
import com.hipnozy.simpleparent.springboot.services.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/globals")
public class GlobalsController {

    @Autowired
    private ColorService colorService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public Map<String, Object> foo() {
        Map<String, Object> globals = new HashMap();
        globals.put("states", StatesEnum.toMap());
        globals.put("colors", colorService.getAllColors());

        return globals;
    }
}
