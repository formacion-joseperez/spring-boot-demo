package com.hipnozy.simpleparent.springboot.enums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The enum States enum.
 */
public enum StatesEnum {

    /**
     * Created states enum.
     */
    CREATED(0),
    /**
     * Payment pending states enum.
     */
    PAYMENT_PENDING(1),
    /**
     * Cancelled states enum.
     */
    CANCELLED(2, true),
    /**
     * Shipped states enum.
     */
    SHIPPED(3, true);

    private Integer key;
    private boolean finished;

    StatesEnum(Integer key) {
        this.key = key;
        this.finished = false;
    }

    StatesEnum(Integer key, boolean finished) {
        this.key = key;
        this.finished = finished;
    }

    /**
     * Gets key.
     *
     * @return the key
     */
    public int getKey() {
        return key;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public String getValue() {
        return String.valueOf(this.name());
    }

    /**
     * Is final boolean.
     *
     * @return the boolean
     */
    public boolean isFinal() {
        return finished;
    }

    /**
     * To map map.
     *
     * @return the map
     */
    public static Map toMap() {
        return Arrays.stream(StatesEnum.values()).collect(Collectors.toMap(
                entry -> String.valueOf(entry.getKey()), entry -> entry.getValue())
        );
    }
}
