package com.hipnozy.simpleparent.springboot.configuration;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DatasourceConfig {
    @Bean
    public DataSource datasource() {
        return DataSourceBuilder.create()
                //.driverClassName("com.mysql.cj.jdbc.Driver")
                .driverClassName("com.mysql.jdbc.Driver")
                .url("jdbc:mysql://localhost/tiendadb?useSSL=false&&serverTimezone=UTC")
                .username("root")
                .password("")
                .build();
    }
}

