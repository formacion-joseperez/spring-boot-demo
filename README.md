
# Crear el proyecto con Maven

Pasos a seguir para crear un proyecto *from scratch*:

1. Crear un directorio con el nombre del proyecto.
2. Crear un fichero **pom.xml** con el parent POM (Podemos usar como plantilla el ejemplo del anexo 1).
3. Crear cada uno de los diferentes submodulos con el comando:

```
mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.3
```

4. Verificar integridad de los POM 

Es importante ejectuarlo en una consola interactiva, maven nos pedirá información.

# Anexo 1. Parent POM

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.sonatype.mavenbook.multi</groupId>
    <artifactId>simple-parent</artifactId>
    <packaging>pom</packaging>
    <version>1.0</version>
    <name>Multi Chapter Simple Parent Project</name>

    <modules>
        <module>simple-weather</module>
        <module>simple-webapp</module>
    </modules>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <source>1.5</source>
                        <target>1.5</target>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>3.8.1</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>
```

# Anexo 2. Compilar y ejecutar el proyecto

Con los siguientes comnandos podemos compilar de cero y arrancar la aplicacion usando el application.properties del directorio conf:

```
$ mvn clean install
$ java -jar simple-parent-spring-boot-web\target\simple-parent-spring-boot-web-1.0-SNAPSHOT.jar --spring.config.location=conf\application.properties
```

# Anexo 3. Generar Spring banner

* [Generador para banner de Spring](https://devops.datenkollektiv.de/banner.txt/index.html)

En el proyecto Web, bajo el directorio **main** debe haber otro **resources**. Se almacena el banner en un fichero llamado **banner.txt**.

# REFERENCIAS

- 